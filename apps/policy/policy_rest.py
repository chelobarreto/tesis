import tornado
from tornado import httpclient
from tesis.rest.base_handler import BaseHandler

class PolicyRest(BaseHandler):
    
    def get(self):
        self.write(self.runtime.getPolicy())
        self.finish()

    def post(self):
        error_data = {}
        error_data['code'] = "ERROR"
        try:
            self.runtime.setPolicy(self.request.body)
        except httpclient.HTTPError as e:
            if (e.response.code == 400):
                self.set_status(400)
                error_data['message'] = "SINTAXIS INCORRECTA"
        except Exception:
            error_data['message'] = "ERROR DESCONOCIDO"
        self.write(error_data)
        self.finish()

    def options(self):
        self.set_status(204)
        self.finish()