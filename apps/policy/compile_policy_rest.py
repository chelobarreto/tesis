import tornado
import json
from tornado import httpclient
from tesis.rest.base_handler import BaseHandler

class CompilePolicyRest(BaseHandler):
	
	def post(self):
		self.set_header("Content-Type", "text/plain")
		error_data = {}
		error_data['code'] = "ERROR"
		try:
			policy_to_compile = self.runtime.getCompilePolicy(self.request.body)
			ret = self.runtime.netkat_compile(policy_to_compile)
			self.write(ret.body)
			self.finish()
		except httpclient.HTTPError as e:
			if (e.response.code == 400):
				self.set_status(400)
				error_data['message'] = "ERROR DE SINTAXIS"
		except Exception:
			error_data['message'] = "ERROR DE DESCONOCIDO"
		self.write(error_data)

	def options(self):
		self.set_status(204)
		self.finish()
