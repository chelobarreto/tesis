import json
import networkx as nx
import tesis.utils.discovery as discovery
from networkx.readwrite import json_graph
import tesis.utils.dpid_utils as dpid_utils

class NetworkInformationBase(object):
	'''
		Estructura de Grafo no dirigido del paquete
		networkx
	'''
	graph = None

	def __init__(self, logger, runtime):
		self.logger = logger
		self.runtime = runtime
		self.graph = nx.Graph()

	def changed(self):
		pass	

	def connected(self, data):
		'''
		data example:
			{
				dpid_1: [port1, .. , portx],
				dpid_2: [port1, .. , portx]
			}

		'''
		for dpid in data:
			self.switch_up(dpid, data[dpid])

	def policy(self):
		return "filter ethTyp = 0x88cc ; port := pipe(\"lldp\")"		

	def to_json(self):
		return json.dumps(json_graph.node_link_data(self.graph))

	def switch_up(self, dpid, ports):
		self.logger.info("Switch %s connected ports: %s", dpid_utils.dpid_to_str(dpid), ports)
		self.graph.add_node(dpid_utils.dpid_to_str(dpid), {'type': 'device', 'ports': []})
		for p in ports:
			self.port_up(dpid, p)		

	def switch_down(self, dpid):
		self.logger.info("Switch %s disconnected", dpid_utils.dpid_to_str(dpid))
		if self.graph.has_node(dpid_utils.dpid_to_str(dpid)):
			self.graph.remove_node(dpid_utils.dpid_to_str(dpid))

	def port_up(self,dpid, port_id):
		self.logger.info("Port %s connected on Switch %s", port_id, dpid_utils.dpid_to_str(dpid))
		if self.graph.has_node(dpid_utils.dpid_to_str(dpid)):
			self.graph.node[dpid_utils.dpid_to_str(dpid)]['ports'].append(port_id)
			discovery.injectDiscoveryPacket(dpid, port_id, self.runtime)

	def port_down(self, dpid, port_id):
		s_dpid = dpid_utils.dpid_to_str(dpid)
		self.logger.info("Port %s disconnected on Switch %s", port_id, s_dpid)
		if self.graph.has_node(s_dpid):
			aristas = self.graph.edges()
			for arista in aristas:
				if arista[0] == s_dpid:
					data = self.graph.get_edge_data(s_dpid, arista[1])
					if data['ports']['source'] == port_id and self.graph.has_edge(s_dpid, arista[1]):
						self.logger.info("Link down detectado (%s,%s)=>(%s,%s)", s_dpid, port_id, arista[1], data['ports']['target'])
						self.graph.remove_edge(s_dpid, arista[1])
				elif arista[1] == s_dpid:
					data = self.graph.get_edge_data(s_dpid, arista[0])
					if data['ports']['target'] == port_id and self.graph.has_edge(s_dpid, arista[0]):
						self.logger.info("Link down detectado (%s,%s)=>(%s,%s)", s_dpid, port_id, arista[0], data['ports']['source'])
						self.graph.remove_edge(s_dpid, arista[0])
						

	def learn_edge(self, src_dpid, src_port, dst_dpid, dst_port):
		src_dpid = dpid_utils.dpid_to_str(src_dpid)
		dst_dpid = dpid_utils.dpid_to_str(dst_dpid)
		self.logger.info("Link detectado (%s,%s)=>(%s,%s)", src_dpid, src_port, dst_dpid, dst_port )
		self.graph.add_edge(src_dpid, dst_dpid, ports={'source': src_port, 'target': dst_port})


	def neighbourDiscovery(self):
		for device in self.graph.nodes:
			for puerto in self.graph.nodes:
				discovery.injectDiscoveryPacket(device, puerto, self.runtime)

	def learnFromDiscoveryPacket(self, switch_id, port_id, payload):
		tupla = discovery.learnFromDiscoveryPacket(payload, self.runtime)
		if tupla:
			self.logger.info("Recibido Paquete LLDP en el Switch %s puerto %s desde el Switch %s puerto %s", switch_id, port_id, tupla[0], tupla[1])
			self.learn_edge(tupla[0], tupla[1], switch_id, port_id)
