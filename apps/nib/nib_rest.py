import logging
from tesis.rest.base_handler import BaseHandler

class NIBRest(BaseHandler):
	
	def get(self):
		self.write(self.runtime.getNib().to_json())
		self.finish()
