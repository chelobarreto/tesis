import sys,logging
from tesis.frenetic.app import App
from tesis.frenetic.packet import *
from tesis.frenetic.syntax import *
from tesis.apps.nib.nib import NetworkInformationBase
from tesis.rest.webserver import WebServer
from tornado.ioloop import IOLoop

class MainApp(App):
	client_id = "tesis"
	user_policy = ""

	def __init__(self):
		super(MainApp,self).__init__()
		self.nib = NetworkInformationBase(logging, self)

	def getNib(self):
		return self.nib

	def getCompilePolicy(self, user_policy_string):
		ret = self.nib.policy()
		if (user_policy_string != ""):
			ret += "\n + \n" + user_policy_string
		return ret

	def getPolicy(self):
		ret = self.nib.policy()
		if (self.user_policy != ""):
			ret += "\n + \n" + self.user_policy
		return ret

	def setPolicy(self, user_policy_string):
		self.setUserPolicy(user_policy_string)
		return self.policyUpdate()

	def setUserPolicy(self, user_policy_string):
		self.user_policy = user_policy_string	

	def getUserPolicy(self):
		return self.user_policy

	def connected(self):
		def handle_current_switches(switches):
			logging.info("Conectado al controlador OpenFlow de Frenetic")
			self.setPolicy("")
			self.nib.connected(switches)
		self.current_switches(callback=handle_current_switches)

	def packet_in(self, switch_id, port_id, payload):
		if (self.packet(payload, "ethernet").ethertype == 0x88cc):			

			logging.debug("LLDP PACKET IN ON SWITCH: %s PORT: %s PAYLOAD: \n %s", switch_id, port_id, payload)
			self.nib.learnFromDiscoveryPacket(switch_id, port_id, payload)
		else:
			pkt = Packet.from_payload(switch_id, port_id, payload)
			logging.info("Evento Packet in Switch: %s puerto: %s from %s to %s", switch_id, port_id, pkt.ethSrc, pkt.ethDst)

	def policyUpdate(self):
		logging.info("Instalando nueva politica: %s", self.getPolicy())
		return super(MainApp,self).netkat_update(self.getPolicy())		

	def switch_up(self,switch_id,ports):
		self.nib.switch_up(switch_id, ports)

	def switch_down(self, switch_id):
		self.nib.switch_down(switch_id)

	def port_up(self,switch_id, port_id):
		self.nib.port_up(switch_id, port_id)

	def port_down(self,switch_id, port_id):
		self.nib.port_down(switch_id, port_id)

if __name__ == '__main__':
	logging.basicConfig(\
		stream = sys.stderr, \
		format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO \
	)
	app = MainApp()
	webserver = WebServer(app, 8080)
	app.start_event_loop()
