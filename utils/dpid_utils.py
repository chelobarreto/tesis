_DPID_LEN = 16
_DPID_FMT = '%0{0}x'.format(_DPID_LEN)
DPID_PATTERN = r'[0-9a-f]{%d}' % _DPID_LEN


def dpid_to_str(dpid):
    return _DPID_FMT % dpid


def str_to_dpid(dpid_str):
    assert len(dpid_str) == _DPID_LEN
    return int(dpid_str, 16)