import logging
from tesis.frenetic.syntax import SetPort, NotBuffered
import tesis.utils.dpid_utils as dpid_utils
from ryu.lib.packet.lldp import *

def injectDiscoveryPacket(dpid, portNumber, runtime):
	import pox.lib.packet as pkt
	chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
	#logging.info("%s %s %s %s", type(dpid), dpid, hex(dpid), (hex(dpid)[2:-1]) )
	chassis_id.id = bytes('dpid:' + dpid_utils.dpid_to_str(dpid))
	port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(portNumber))
	ttl = pkt.ttl(ttl = 120)
	sysdesc = pkt.system_description()
	sysdesc.payload = chassis_id.id
	discovery_packet = pkt.lldp()
	discovery_packet.tlvs.append(chassis_id)
	discovery_packet.tlvs.append(port_id)
	discovery_packet.tlvs.append(ttl)
	discovery_packet.tlvs.append(sysdesc)
	discovery_packet.tlvs.append(pkt.end_tlv())
	eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
	eth.dst = pkt.ETHERNET.NDP_MULTICAST
	eth.payload = discovery_packet
	nb = NotBuffered(eth.pack())
	runtime.pkt_out(dpid,nb,SetPort(portNumber))
	logging.info("INJECTANDO PAQUETE DE DISCOVERY EN SWITCH: %s PUERTO: %d", dpid, portNumber)

def learnFromDiscoveryPacket(payload, runtime):
	lldp = runtime.packet(payload, "lldp")
	for o in lldp.tlvs:
		if (o.tlv_type == LLDP_TLV_SYSTEM_DESCRIPTION):
			originatorDPID = "".join(map(chr,o.tlv_info))
			if originatorDPID.startswith('dpid:'):
				originatorDPID = dpid_utils.str_to_dpid(originatorDPID[5:])
				#originatorDPID = int(originatorDPID[5:], 16)
		elif (o.tlv_type == LLDP_TLV_PORT_ID):
			originatorPort = int("".join(map(chr,o.port_id)))
	if (originatorDPID == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR DPID NULL")
		return None
	if (originatorPort == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR PORT NULL")
		return None
	logging.info("DPID O: %s DPID PORT: %s", originatorDPID, originatorPort)
	return (originatorDPID, originatorPort)