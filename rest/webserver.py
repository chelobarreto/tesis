import logging
import tornado.web
from tesis.apps.nib.nib_rest import NIBRest
from tesis.apps.policy.policy_rest import PolicyRest
from tesis.apps.policy.compile_policy_rest import CompilePolicyRest

class WebServer(object):
	def __init__(self, runtime, port):
		logging.info("STARTING REST WEB SERVER")
		self.runtime = runtime
		handlers = [
			(r'/nib', NIBRest, {'runtime': self.runtime}),
			(r'/policy', PolicyRest, {'runtime': self.runtime}),
			(r'/compile_policy', CompilePolicyRest, {'runtime': self.runtime})
		]
		self.app = tornado.web.Application(handlers)
		self.app.listen(port)
		logging.info("REST WEB SERVER STARTED")