import logging
import tornado.web
from tornado import httpclient

class BaseHandler(tornado.web.RequestHandler):
    
    def initialize(self, runtime):
        self.runtime = runtime
        self.logger = logging
    
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Headers, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding ")
        self.set_header('Access-Control-Allow-Methods', 'Content-TypeNECT, POST, GET, DELETE, OPTIONS, PUT')