import json, logging
from tesis.model.device.device import OpenFlowDevice
import ryu.lib.dpid as utils

class NIB(object):
	
	def __init__(self, logging):
		self.devices = {}
		self.dirty = False

	def isDirty(self):
		return self.dirty

	def setDirty(self):
		self.dirty = True

	def clearDirty(self):
		self.dirty = False

	def getDevices(self):
		return self.devices.values()

	def getDevice(self, dpid):
		return self.devices[dpid]

	def switchUp(self, dpid, switchFeatures):
		self.devices[dpid] = OpenFlowDevice(dpid, switchFeatures)
		self.setDirty()

	def switchDown(self, dpid):
		del self.devices[dpid]
		self.setDirty()

	def portUp(self, dpid, portDescription):
		self.devices[dpid].portUp(portDescription)
		self.setDirty()

	def portDown(self, dpid, portNumber):
		self.devices[dpid].portDown(portNumber)
		self.setDirty()

	def addLink(self, o_dpid, o_port, d_dpid, d_port):
		self.devices[o_dpid].addLink(o_port, d_dpid, d_port)
		self.devices[d_dpid].addLink(d_port, o_dpid, o_port)
		logging.info("LINK FROM %s@%d TO %s@%d", o_dpid, o_port, d_dpid, d_port)
	
	def to_json(self):
		nodes = []
		links = []
		for device in self.devices:
			nodes.append({'dpid': utils.dpid_to_str(device)})
			for link in self.devices[device].links:
				(d_dpid, d_port) = self.devices[device].links[link]
				links.append({'source': utils.dpid_to_str(device), 'target': utils.dpid_to_str(d_dpid)})
		return {'nodes': nodes, 'links': links}

	def __repr__(self):
		ret = ""
		for device in self.devices:
			ret += "\t* " + str(self.devices[device])
		return ret