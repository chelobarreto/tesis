import ryu.lib.dpid as utils

class Port(object):
	def __init__(self, deviceOwner, portDescription):
		self.device = deviceOwner
		self.portDescription = PortDescription(portDescription)

	def portUp(self):
		self.portDescription.state.down = False

	def portDown(self):
		self.portDescription.state.down = True

	def isUp(self):
		return not self.isDown()

	def isDown(self):
		return self.portDescription.state.down

	def getPortNumber(self):
		return self.portDescription.port_no

	def to_json(self):
		return self.portDescription.to_json()

	def __repr__(self):
		ret = "\t\t* PORT ID: " + str(self.portDescription.port_no) + "\n"
		ret += "\t\t\t* DOWN: " + self.isDown() + "\n"
		return ret

class OpenFlowDevice(object):
	def __init__(self, dpid, switchFeatures):
		self.dpid = dpid
		self.switchFeatures = SwitchFeatures(switchFeatures)
		self.ports = {}
		for port in switchFeatures['ports']:
			if (port['port_no'] != 65534):
				self.addPort(port)
		self.links = {}

	def getPorts(self):
		return self.ports.values()

	def getLinks(self):
		return self.links.values()

	def getPort(self, portNumber):
		return self.ports[portNumber]

	def addPort(self, portDescription):
		self.ports[portDescription['port_no']] = Port(self, portDescription)

	def getPort(self, portNumber):
		return self.ports[portNumber]

	def portUp(self, portDescription):
		if (portDescription['port_no'] != 65534):
			self.ports[portDescription['port_no']].portUp()

	def portDown(self, portNumber):
		self.ports[portNumber].portDown()

	def addLink(self, port, destDPID, destPortNo):
		self.links[port] = (destDPID, destPortNo)

	def to_json(self):
		ports = []
		for port in self.ports.values():
			ports.append(port.to_json())
		return {
			'dpid': utils.dpid_to_str(self.dpid),
			'num_buffers' : self.switchFeatures.num_buffers,
			'num_tables' : self.switchFeatures.num_tables,
			'ports': ports
		}

	def __repr__(self):
		ret = "DEVICE ID: " + str(self.dpid) + "\n"
		for port in self.ports:
			ret += str(self.ports[port])
		return ret


'''
	OPENFLOW REPRESENTATIONS
'''
class PortState(object):
	def __init__(self, jsonPortState):
		self.down = jsonPortState['down']
		self.stpState = jsonPortState['stp_state']

	def to_json(self):
		return {
			'down' : self.down,
			'stp_state': self.stpState
		}

class PortConfig(object):
	def __init__(self, jsonPortConfig):
		self.down = jsonPortConfig['down']
		self.no_stp = jsonPortConfig['no_stp']
		self.no_recv = jsonPortConfig['no_recv']
		self.no_recv_stp = jsonPortConfig['no_recv_stp']
		self.no_flood = jsonPortConfig['no_flood']
		self.no_fwd = jsonPortConfig['no_fwd']
		self.no_packet_in = jsonPortConfig['no_packet_in']

	def to_json(self):
		return {
			'down': self.down,
			'no_stp': self.no_stp,
			'no_recv': self.no_recv,
			'no_recv_stp': self.no_recv_stp,
			'no_flood': self.no_flood,
			'no_fwd': self.no_fwd,
			'no_packet_in': self.no_packet_in
		}

class PortFeatures(object):
	def __init__(self, jsonPortFeatures):
		self.f_10MBHD = jsonPortFeatures['f_10MBHD']
		self.f_10MBFD = jsonPortFeatures['f_10MBFD']
		self.f_100MBHD = jsonPortFeatures['f_100MBHD']
		self.f_100MBFD = jsonPortFeatures['f_100MBFD']
		self.f_1GBHD = jsonPortFeatures['f_1GBHD']
		self.f_1GBFD = jsonPortFeatures['f_1GBFD']
		self.f_10GBFD = jsonPortFeatures['f_10GBFD']
		self.copper = jsonPortFeatures['copper']
		self.fiber = jsonPortFeatures['fiber']
		self.autoneg = jsonPortFeatures['autoneg']
		self.pause = jsonPortFeatures['pause']
		self.pause_asym = jsonPortFeatures['pause_asym']

	def to_json(self):
		ret = {}
		for attr, value in self.__dict__.iteritems():
			if (value == 'true'):
				ret[attr] = value
		return ret


class PortDescription(object):
	def __init__(self, jsonPortDescription):
		self.port_no = jsonPortDescription['port_no']
		self.hw_addr = jsonPortDescription['hw_addr']
		self.name = jsonPortDescription['name']
		self.state = PortState(jsonPortDescription['state'])
		self.config = PortConfig(jsonPortDescription['config'])
		self.curr = PortFeatures(jsonPortDescription['curr'])
		self.supported = PortFeatures(jsonPortDescription['supported'])
		self.advertised = PortFeatures(jsonPortDescription['advertised'])
		self.peer = PortFeatures(jsonPortDescription['peer'])

	def to_json(self):
		return {
			'port_no' : self.port_no,
			'hw_addr' : self.hw_addr,
			'name' : self.name,
			'state': self.state.to_json(),
			'config' :	self.config.to_json(),
			'curr' : self.curr.to_json(),
			'supported' : self.supported.to_json(),
			'advertised' : self.advertised.to_json(),
			'peer': self.peer.to_json()
		}

class SwitchFeatures(object):
	def __init__(self, jsonSwitchFeatures):
		self.num_buffers = jsonSwitchFeatures['num_buffers']
		self.num_tables = jsonSwitchFeatures['num_tables']