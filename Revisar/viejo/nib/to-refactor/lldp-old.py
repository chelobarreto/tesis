import pox.lib.packet as pkt
from tesis.frenetic.packet import *
from tesis.frenetic.syntax import SetPort
from ryu.lib.packet.lldp import *
import logging


class LLDP(object):
	def __init__(self, dpid, portNumber):
		chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
		chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])
		port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(portNumber))
		ttl = pkt.ttl(ttl = 120)
		sysdesc = pkt.system_description()
		sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])
		discovery_packet = pkt.lldp()
		discovery_packet.tlvs.append(chassis_id)
		discovery_packet.tlvs.append(port_id)
		discovery_packet.tlvs.append(ttl)
		discovery_packet.tlvs.append(sysdesc)
		discovery_packet.tlvs.append(pkt.end_tlv())
		eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
		eth.dst = pkt.ETHERNET.NDP_MULTICAST
		eth.payload = discovery_packet
		nb = NotBuffered(eth.pack())
		return nb

def handle_lldp(lldp, dpid, port_id, nib):
	for o in lldp.tlvs:
		if (o.tlv_type == LLDP_TLV_SYSTEM_DESCRIPTION):
			originatorDPID = "".join(map(chr,o.tlv_info))
			if originatorDPID.startswith('dpid:'):
				originatorDPID = int(originatorDPID[5:], 16)
		elif (o.tlv_type == LLDP_TLV_PORT_ID):
			originatorPort = int("".join(map(chr,o.port_id)))

	if (originatorDPID == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR DPID NULL")
		return
	if (originatorPort == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR PORT NULL")
		return
	logging.info("ORIG %s OPORT %d", originatorDPID, originatorPort)
	#nib.addLink(originatorDPID, originatorPort, dpid, port_id)