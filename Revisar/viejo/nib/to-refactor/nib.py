from tesis.frenetic.syntax import *
from tesis.network.device import OpenFlowDevice

class NIB(object):
	
	def __init__(self, runtime):
		self.devices = {}
		self.runtime = runtime

	def switchUp(self, dpid, switchFeatures):
		self.devices[dpid] = OpenFlowDevice(dpid, switchFeatures)

	def switchDown(self, dpid):
		del self.devices[dpid]

	def portUp(self, dpid, portDescription):
		self.devices[dpid].portUp(portDescription)
		self.runtime.injectDiscoveryPacket(dpid, portDescription['port_no'])

	def portDown(self, dpid, portNumber):
		self.devices[dpid].portDown(portNumber)

	def neighbourDiscovery(self, dpid, portNumber)
		self.runtime.pkt_out(dpid,createLLDPPacket(dpid,portNumber),SetPort(portNumber))

	def neighbourResponse(self, dpid, portNumber, payload):
		logging.info("Neighbour Response: %s@%s", % fromLLDPPacket(payload))

	def __repr__(self):
		ret = ""
		for device in self.devices:
			ret += "\t* " + str(self.devices[device])
		return ret



def createLLDPPacket(dpid, portNumber):
	import pox.lib.packet as pkt
	chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
	chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])
	port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(portNumber))
	ttl = pkt.ttl(ttl = 120)
	sysdesc = pkt.system_description()
	sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])
	discovery_packet = pkt.lldp()
	discovery_packet.tlvs.append(chassis_id)
	discovery_packet.tlvs.append(port_id)
	discovery_packet.tlvs.append(ttl)
	discovery_packet.tlvs.append(sysdesc)
	discovery_packet.tlvs.append(pkt.end_tlv())
	eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
	eth.dst = pkt.ETHERNET.NDP_MULTICAST
	eth.payload = discovery_packet
	nb = NotBuffered(eth.pack())
	return nb

def fromLLDPPacket(payload):
	lldp = self.packet(payload, "lldp")
	for o in lldp.tlvs:
		if (o.tlv_type == LLDP_TLV_SYSTEM_DESCRIPTION):
			originatorDPID = "".join(map(chr,o.tlv_info))
			if originatorDPID.startswith('dpid:'):
				originatorDPID = int(originatorDPID[5:], 16)
		elif (o.tlv_type == LLDP_TLV_PORT_ID):
			originatorPort = int("".join(map(chr,o.port_id)))

	if (originatorDPID == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR DPID NULL")
		return
	if (originatorPort == None):
		logging.error("LINK DISCOVERY FAIL ORIGINATOR PORT NULL")
		return
	return (originatorDPID, originatorPort)