from tesis.network.device import OpenFlowDevice
import json
class Link(object):
	def __init__(self, src_id, src_port, dst_id, dst_port):
		self.src = src_id
		self.srcPort = src_port
		self.dst = dst_id
		self.dstPort = dst_port		

	def __hash__(self):
		return hash((self.src, self.srcPort, self.dst, self.dstPort))

	def __repr__(self):
		return "LINK FROM: %s PORT: %s TO %s PORT: %s\n" % (self.src, self.srcPort, self.dst, self.dstPort)


class NIB(object):
	def __init__(self, runtime):
		self.devices = {}
		self.links = {}
		self.runtime = runtime

	def switchUp(self, dpid, feats):
		self.devices[dpid] = OpenFlowDevice(dpid, feats)

	def switchDown(self, dpid):
		try:
			del self.devices[dpid]
		except KeyError:
			logging.info("Try to remove unexistent device: %s", dpid)

	def portUp(self, dpid, portDescription):
		#PORT TO CONTROLLER
		if (portDescription['port_no'] != 65534):
			self.devices[dpid].portUp(portDescription)
			#@TODO DISCOVER LINKS

	def portDown(self, dpid, portNumber):
		self.devices[dpid].portDown(portDescription)
'''
	def getDevice(self, dpid):
		return self.devices[dpid]

	
	def removeDevice(self, dpid):
		try:
			del self.devices[dpid]
		except KeyError:
			pass

	def addPort(self, dpid, portFeatures):
		self.devices[dpid].addPort(portFeatures)
		if (portFeatures['port_no'] != 65534):
			self.runtime.injectDicoveryPacket(dpid, portFeatures['port_no'])

	def addLink(self, srcDPID, srcPortNumber, dstDPID, dstPortNumber):
		l = Link(srcDPID, srcPortNumber, dstDPID, dstPortNumber)	
		self.links[l] = l

	def to_json(self):
		ret = {}
		nodes = []
		links = []
		for d in self.devices:
			n = { 'id': self.devices[d].dpid , 'dpid' : self.devices[d].dpid }
			nodes.append(n)
		ret['nodes'] = nodes
		for l in self.links:
			r = {'source' : self.links[l].src, 'target': self.links[l].dst}
			links.append(r)
		ret['links'] = links
 		return json.dumps(ret)

	def __repr__(self):
		ret = "\n"
		for d in self.devices:
			ret += str(self.devices[d])
		for l in self.links:
			ret += str(self.links[l])
		return ret
'''