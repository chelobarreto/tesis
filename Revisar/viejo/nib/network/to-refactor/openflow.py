class PortState(object):
	def __init__(self, jsonPortState):
		self.down = jsonPortState['down']
		self.stpState = jsonPortState['stp_state']

class PortConfig(object):
	def __init__(self, jsonPortConfig):
		self.down = jsonPortConfig['down']
		self.no_stp = jsonPortConfig['no_stp']
		self.no_recv = jsonPortConfig['no_recv']
		self.no_recv_stp = jsonPortConfig['no_recv_stp']
		self.no_flood = jsonPortConfig['no_flood']
		self.no_fwd = jsonPortConfig['no_fwd']
		self.no_packet_in = jsonPortConfig['no_packet_in']

class PortFeatures(object):
	def __init__(self, jsonPortFeatures):
		self.f_10MBHD = jsonPortFeatures['f_10MBHD']
		self.f_10MBFD = jsonPortFeatures['f_10MBFD']
		self.f_100MBHD = jsonPortFeatures['f_100MBHD']
		self.f_100MBFD = jsonPortFeatures['f_100MBFD']
		self.f_1GBHD = jsonPortFeatures['f_1GBHD']
		self.f_1GBFD = jsonPortFeatures['f_1GBFD']
		self.f_10GBFD = jsonPortFeatures['f_10GBFD']
		self.copper = jsonPortFeatures['copper']
		self.fiber = jsonPortFeatures['fiber']
		self.autoneg = jsonPortFeatures['autoneg']
		self.pause = jsonPortFeatures['pause']
		self.pause_asym = jsonPortFeatures['pause_asym']

class PortDescription(object):
	def __init__(self, jsonPortDescription):
		self.port_no = jsonPortDescription['port_no']
		self.hw_addr = jsonPortDescription['hw_addr']
		self.name = jsonPortDescription['name']
		self.state = PortState(jsonPortDescription['state'])
		self.config = PortConfig(jsonPortDescription['config'])
		self.curr = PortFeatures(jsonPortDescription['curr'])
		self.supported = PortFeatures(jsonPortDescription['supported'])
		self.advertised = PortFeatures(jsonPortDescription['advertised'])
		self.peer = PortFeatures(jsonPortDescription['peer'])

class SwitchFeatures(object):
	def __init__(self, jsonSwitchFeatures):
		self.num_buffers = jsonSwitchFeatures['num_buffers']
		self.num_tables = jsonSwitchFeatures['num_tables']