from tesis.network.openflow import PortDescription, SwitchFeatures
class Port(object):
	def __init__(self, deviceOwner, portDescription):
		self.device = deviceOwner
		self.portDescription = PortDescription(portDescription)

	def portUp(self):
		self.portDescription.state.down = False

	def portDown(self):
		self.portDescription.state.down = True

	def isUp(self):
		return not self.isDown()

	def isDown(self):
		return self.portDescription.state.down

	def getPortNumber(self):
		return self.portDescription.port_no

	def __repr__(self):
		ret = "\t\t* PORT ID: " + str(self.portDescription.port_no) + "\n"
		ret += "\t\t\t* DOWN: " + self.isDown() + "\n"
		return ret

class OpenFlowDevice(object):
	def __init__(self, dpid, switchFeatures):
		self.dpid = dpid
		self.switchFeatures = SwitchFeatures(switchFeatures)
		self.ports = {}
		for port in switchFeatures['ports']:
			if (port['port_no'] != 65534):
				self.addPort(port)

	def addPort(self, portDescription):
		self.ports[portDescription['port_no']] = Port(self, portDescription)

	def getPort(self, portNumber):
		return self.ports[portNumber]

	def portUp(self, portDescription):
		self.ports[portDescription['port_no']].portUp()

	def portDown(self, portNumber):
		self.ports[portDescription['port_no']].portDown()

	def __repr__(self):
		ret = "DEVICE ID: " + str(self.dpid) + "\n"
		for port in self.ports:
			ret += str(self.ports[port])
		return ret


'''
import json
class Port(object):
	def __init__(self, portNumber, deviceOwner):
		self.portNumber = portNumber
		self.deviceOwner = deviceOwner

	def setFeatures(self, portFeatures):
		self.hw_addr = portFeatures['hw_addr']
		self.name = portFeatures['name']
		self.state = portFeatures['state']
		self.curr = portFeatures['curr']
		self.supported = portFeatures['supported']
		self.advertised = portFeatures['advertised']
		self.peer = portFeatures['peer']

	def isUp(self):
		print self.curr

	def setDown(self):
		print self.curr
	
	def to_json(self):
		return {
			'hw_addr': self.hw_addr,
			'port_number' : self.portNumber,
			'name': self.name,
			'state': self.state			
			}

	def __repr__(self):
		return "\t\tPortNumber: " + str(self.portNumber)

class OpenFlowDevice(object):
	def __init__(self, dpid, feats):
		self.dpid = dpid
		self.ports = {}
		self.num_buffers = feats['num_buffers']
		self.num_tables = feats['num_tables']
		for portDescription in feats['ports']:
			self.addPort(portDescription)

	def portDown(self,dpid,  portNumber):
		self.devices[dpid].portDown(portNumber)

	def portUp(self, portDescription):
		self.ports[portDescription['port_no']].setFeatures(portDescription)

	def addPort(self, portFeatures):
		if (portFeatures['port_no'] != 65534):
			self.ports[portFeatures['port_no']] = Port(portFeatures['port_no'], self)
			self.ports[portFeatures['port_no']].setFeatures(portFeatures)

	def to_json(self):
		ret = { 'dpid' : self.dpid }
		ret_ports = []
		for p in self.ports:
			ret_ports.append(self.ports[p].to_json())
		ret['ports'] = ret_ports
		return json.dumps(ret)

	def __repr__(self):
		ret = "\tDPID: " + str(self.dpid) + "\n"
		for p in self.ports:
			ret += str(self.ports[p]) + "\n"
		return ret + "\n"
'''