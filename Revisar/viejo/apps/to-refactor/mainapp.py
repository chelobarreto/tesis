from tesis.frenetic.app import App
from tesis.frenetic.syntax import *
from tesis.frenetic.packet import *
from tesis.rest.webserver import WebServer
from tesis.nib.nib import NIB
from tesis.nib.lldp import LLDP, handle_lldp
import logging, sys, json

class MainApp(App):
	client_id = "tesis"
	
	def __init__(self):
		super(MainApp,self).__init__()
		self.nib = NIB(self)
		self.policy = None

	def injectDiscoveryPacket(self, dpid, portNumber):
		logging.info("INJECT A DISCOVERY PACKET %s@%s", dpid, portNumber)
		LLDP(dpid, portNumber, self)
		logging.info("LLDP SEND FROM: %s PORT: %s" %(dpid, portNumber))

	def handle_current_switches(self, switches):
		for dpid in switches:
			switchFeatures = switches[dpid]
			self.nib.switchUp(dpid, switchFeatures)
		print self.nib
		for dpid in self.nib.devices:
			for port in self.nib.devices[dpid].ports:
				self.injectDiscoveryPacket(dpid, port)

	def initial_policy(self):
		return IfThenElse(EthTypeEq(35020), SendToController("lldp"), SendToController("all"))
		
	def connected(self):
		logging.info("CONNECTED")
		print "UPDATE: " , self.update(self.initial_policy())
		logging.info("INSTALLED DEFAULT POLICY")
		self.current_switches(callback=self.handle_current_switches)

	def switch_up(self, dpid, feats):
		logging.info("SWITCH UP")

	def switch_down(self, dpid):
		logging.info("SWITCH DOWN")

	def port_up(self,dpid, portDescription):
		logging.info("PORT UP")
		self.nib.portUp(dpid, portDescription)

	def port_down(self,dpid, portNumber):
		logging.info("PORT DOWN")

	def packet_in(self, dpid, port_id, payload, pipe):
		if (pipe == 'lldp'):
			logging.info("NIB PIPE HANDLER FOR PACKET IN: %s", pipe)
			self.nib
		elif (pipe == 'all'):
			logging.info("ALL PIPE HANDLER FOR PACKET IN: %s", pipe)
		else:
			logging.info("NO PIPE HANDLER FOR PACKET IN: %s", pipe)

logging.basicConfig(stream = sys.stderr, \
format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO \
)
app = MainApp()
#webserver = WebServer(app, 9090)
app.start_event_loop()