import frenetic, logging, sys, json
from frenetic.syntax import *
from tesis.nib.nib import NIB
from tesis.nib.util import LLDP, handle_lldp
from tesis.apps.webserver import WebServer
from tesis.apps.baseapp import App

class MainApp(App):
	client_id = "tesis"

	def __init__(self):
		super(MainApp,self).__init__()
		self.nib = NIB(self)

	def injectDicoveryPacket(self, dpid, portNumber):
		LLDP(dpid,portNumber,self)

	def handle_current_switches(self, switches):
		logging.info("===============NEW NIB==================")
		self.nib = NIB(self)
		for d, feats in switches.items():
			self.nib.addDevice(d, feats['num_buffers'], feats['num_tables'])
			for p in feats['ports']:
				print self.nib.addPort(d, p)

	def connected(self):
		logging.info("===============CONNECTED==================")
		self.current_switches(callback=self.handle_current_switches)
		self.update( id >> SendToController('tesis') )

	def packet_in(self, dpid, port_id, payload):
		ethernet_packet = self.packet(payload, "ethernet")
		if (ethernet_packet.ethertype == 35020):
			handle_lldp(self.packet(payload, "lldp"), dpid, port_id, self.nib)
			#logging.info(self.nib)
		self.pkt_out(dpid, payload, SetPort(2) )

logging.basicConfig(stream = sys.stderr, \
format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO \
)

app = MainApp()
webserver = WebServer(app, 9090)
app.start_event_loop()
