from tesis.rest.base_handler import BaseHandler
class DeviceRest(BaseHandler):
		
	def get(self, dpid):
		self.write(self.runtime.nib.getDevice(int(dpid)).to_json())
		self.finish()
