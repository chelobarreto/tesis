import logging
import tornado
from tesis.rest.nib_rest import NIBRest
from tesis.rest.device_rest import DeviceRest
class WebServer(object):
	def __init__(self, runtime, port):
		logging.info("STARTING REST WEB SERVER")
		self.runtime = runtime
		self.app = tornado.web.Application()
		'''self.app = tornado.web.Application(
			[
				(r'/nib', NIBRest, {'runtime': self.runtime}),
				(r'/device/([0-9]+)', DeviceRest, {'runtime': self.runtime})
			],)'''
		self.app.listen(9090)
		logging.info("REST WEB SERVER STARTED")