import sys,logging, datetime
from tesis.frenetic.app import App
from tesis.frenetic.packet import *
from tesis.model.nib.nib import NIB
from tesis.modules.nib.nib_handler import NIBHandler
from tesis.rest.webserver import WebServer
from tornado.ioloop import IOLoop

class MainApp(App):
	client_id = "tesis"

	def __init__(self):
		super(MainApp,self).__init__() 
		self.nib = NIB(logging)
		self.nib_handler =  NIBHandler(self.nib, logging, self)
		self.initialized = False

	def policy(self):
		return Union([
			self.nib_handler.policy()
			]
			)

	def update_and_clear_dirty(self):
		logging.info("INSTALLING NEW POLICY")
		self.update(self.policy())
		self.nib.clearDirty()

	def connected(self):
		def handle_current_switches(switches):
			logging.info("CONNECTED TO FRENETIC")
			self.nib_handler.connected(switches)
			self.update_and_clear_dirty()
		self.current_switches(callback=handle_current_switches)

	def packet_in(self, dpid, port, payload, pipe):
		if (not self.initialized):
			self.nib_handler.neighbourLookup()
			self.initialized = True
		if (pipe == 'lldp' or self.packet(payload, "ethernet").ethertype == 0x88cc):
			logging.info("NIB PIPE HANDLER FOR PACKET IN: %s", pipe)
			self.nib_handler.packet_in(dpid, port, payload)
		elif (pipe == 'all'):
			logging.info("ALL PIPE HANDLER FOR PACKET IN: %s", pipe)
		else:
			logging.info("NO PIPE HANDLER FOR PACKET IN: %s", pipe)
		if self.nib.isDirty():
			
			# This doesn't actually wait two seconds, but it serializes the updates 
			# so they occur in the right order
			IOLoop.instance().add_timeout(datetime.timedelta(seconds=2), self.update_and_clear_dirty)
	
	
	def switch_up(self, dpid, feats):
		logging.info("SWITCH UP")
		self.nib_handler.switchUp(dpid, feats)
		self.update_and_clear_dirty()

	def switch_down(self, dpid):
		logging.info("SWITCH DOWN")
		self.nib_handler.switchDown(dpid)
		self.update_and_clear_dirty()

	def port_up(self, dpid, portDescription):
		logging.info("PORT UP")
		self.nib_handler.portUp(dpid, portDescription)
		self.update_and_clear_dirty()

	def port_down(self, dpid, portNumber):
		logging.info("PORT DOWN")
		self.nib_handler.portDown(dpid, portNumber)
		self.update_and_clear_dirty()

if __name__ == '__main__':
	logging.basicConfig(\
		stream = sys.stderr, \
		format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO \
	)
	app = MainApp()
	webserver = WebServer(app, 9090)
	app.start_event_loop()