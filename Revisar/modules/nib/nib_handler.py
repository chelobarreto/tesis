import time
from tesis.frenetic.syntax import *
from tesis.frenetic.packet import *
from ryu.lib.packet.lldp import *
from tornado.ioloop import PeriodicCallback
LLDP_ETHTYPE = 0X88cc

class NIBHandler(object):

	def __init__(self, nib, logger, main_app):
		self.nib = nib
		self.logger = logger
		self.main_app = main_app

	def neighbourLookup(self):
		for device in self.nib.getDevices():
			for port in device.getPorts():
				self.injectDiscoveryPacket(device.dpid, port.getPortNumber())

	def connected(self, switches):
		for dpid in switches:
			switchFeatures = switches[dpid]
			self.nib.switchUp(dpid, switchFeatures)

	def policy(self):
		return IfThenElse(EthTypeEq(LLDP_ETHTYPE), SendToController("lldp"), SendToController("all"))

	def packet_in(self, dpid, port, payload):
		(ndpid, nport_no) = self.learnNeighbour(payload)
		self.nib.addLink(ndpid, nport_no, dpid, port)

	def switchUp(self, dpid, switchFeatures):
		self.nib.switchUp(dpid, switchFeatures)
		s = self.nib.getDevice(dpid)
		for port in s.getPorts():
			self.injectDiscoveryPacket(dpid, port.getPortNumber())

	def switchDown(self, dpid):
		self.nib.switchDown(dpid)

	def portUp(self, dpid, portDescription):
		self.nib.portUp(dpid, portDescription)
		self.injectDiscoveryPacket(dpid,portDescription['port_no'])

	def portDown(self, dpid, portNumber):
		self.nib.portDown(dpid, portNumber)

	def injectDiscoveryPacket(self, dpid, portNumber):
		import pox.lib.packet as pkt
		chassis_id = pkt.chassis_id(subtype=pkt.chassis_id.SUB_LOCAL)
		chassis_id.id = bytes('dpid:' + hex(long(dpid))[2:-1])
		port_id = pkt.port_id(subtype=pkt.port_id.SUB_PORT, id=str(portNumber))
		ttl = pkt.ttl(ttl = 120)
		sysdesc = pkt.system_description()
		sysdesc.payload = bytes('dpid:' + hex(long(dpid))[2:-1])
		discovery_packet = pkt.lldp()
		discovery_packet.tlvs.append(chassis_id)
		discovery_packet.tlvs.append(port_id)
		discovery_packet.tlvs.append(ttl)
		discovery_packet.tlvs.append(sysdesc)
		discovery_packet.tlvs.append(pkt.end_tlv())
		eth = pkt.ethernet(type=pkt.ethernet.LLDP_TYPE)
		eth.dst = pkt.ETHERNET.NDP_MULTICAST
		eth.payload = discovery_packet
		nb = NotBuffered(eth.pack())
		self.main_app.pkt_out(dpid,nb,SetPort(portNumber))
		self.logger.info("DISCOVERY PACKET INJECTED %s@%d", dpid, portNumber)

	def learnNeighbour(self, payload):
		lldp = self.main_app.packet(payload, "lldp")
		for o in lldp.tlvs:
			if (o.tlv_type == LLDP_TLV_SYSTEM_DESCRIPTION):
				originatorDPID = "".join(map(chr,o.tlv_info))
				if originatorDPID.startswith('dpid:'):
					originatorDPID = int(originatorDPID[5:], 16)
			elif (o.tlv_type == LLDP_TLV_PORT_ID):
				originatorPort = int("".join(map(chr,o.port_id)))
		if (originatorDPID == None):
			logging.error("LINK DISCOVERY FAIL ORIGINATOR DPID NULL")
			return
		if (originatorPort == None):
			logging.error("LINK DISCOVERY FAIL ORIGINATOR PORT NULL")
			return
		return (originatorDPID, originatorPort)
		