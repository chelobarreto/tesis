=================================================SINTAXIS NETKAT=============================================
http://frenetic-lang.github.io/tutorials/NetKATManual/

===========TIPOS
Types:

(* Integers can be either decimal or hexadecimal (with leading 0x *)

<mac-address> ::= xx:xx:xx:xx:xx:xx
<ip-address> ::= xxx.xxx.xxx.xxx
<mask> = 1 ... 32
<masked-ip-address> ::= <ip-address> / <mask>
<switch-id> ::= 64-bit integer
<port-id> ::= 16-bit integer
<vport-id> ::= 64-bit integer
<vfrabric-id> ::= 64-bit integer
<vlan-id> ::= none | 12-bit integer
<tcp-port> ::= 16-bit integer
<vlan-pcp> ::= 16-bit integer
<frame-type> ::= arp (* shorthand for 0x806 *)
               | ip  (* shorthand for 0x800 *)
               | 8-bit integer
<ip-protocol> ::= icmp (* shorthand for 0x01 *)
                | tcp  (* shorthand for 0x06 *)
                | udp  (* shorthand for 0x11 *)
                | 8-bit integer
<location> ::= <switch-id> @ <port-id>

===========PREDICATE

<pred-atom> ::= ( <pred> )
            | true
            | false
            | switch = <switch-id>
            | port = <port-id>
            | vswitch = <switch-id>
            | vport = <vport-id>
            | vfabric = <vfabric-id>
            | vlanId = <vlan-id>
            | vlanPcp = <vlan-pcp>
            | ethTyp = <frame-type>
            | ipProto = <ip-protocol>
            | tcpSrcPort = <tcp-port>
            | tcpDstPort = <tcp-port>
            | ethSrc = <mac-address>
            | ethDst = <mac-address>
            | ip4Src = <masked-ip-address> | <ip-address>
            | ip4Dst = <masked-ip-address> | <ip-address>

<not-pred> ::= <pred-atom>
            | not <not-pred>

<and-pred> ::= <not-pred>
            | <and-pred> and <not-pred>

<or-pred> ::= <and-pred>
           | <or-pred> or <and-pred>

<pred> ::= <or-pred>

============POLICY
<pol-atom> ::= ( <pol> )
           | id
           | drop
           | filter <pred>
           | switch := <switch-id>
           | port := <port-id>
           | vswitch := <switch-id>
           | vport := <vport-id>
           | vfabric := <vfabric-id>
           | vlanId := <vlan-id>
           | vlanPcp := <vlan-pcp>
           | ethTyp := <frame-type>
           | ipProto := <ip-protocol>
           | tcpSrcPort := <tcp-port>
           | tcpDstPort := <tcp-port>
           | ethSrc := <mac-address>
           | ethDst := <mac-address>
           | ip4Src := <ip-address>
           | ip4Dst := <ip-address>
           | <location> => <location>
           | <location> =>> <location>

<star-pol> ::= <pol-atom> 
            | <star-pol> *

<seq-pol> ::= <star-pol>
            | <seq-pol> ; <star-pol>

<union-pol> ::= <seq-pol>
              | <union-pol> + <seq-pol>

<cond-pol> ::= <union-pol>
            | if <pred> then <cond-pol> else <cond-pol>

<pol> ::= <cond-pol>

<program> ::= <pol>



REVISAR begin, end, pipe , query
=================================================PRINCIPIOS=============================================

Principle 1:
	Keep as much traffic out of the controller as possible. Instead, program NetKAT
	policies to make most of the decisions inside the switch.
Principle 2:
	Use >> between filters and all actions except multiple SetPorts.
Principle 3:
	Use | between multiple SetPorts and rules that DO NOT overlap. 
	Use IfThenElse to combine rules that DO overlap.
Principle 4:
	When you install a new switch policy, do not assume it’ll be installed right away.
Principle 5:
	Do not rely on network state. Always assume the current packet is the first one
	you’re seeing.


======================================================JSON===================================================

* portState:
	
	down: boolean				/* No physical link present. */
	
	stp_state: ["LISTEN"|"LEARN"|"FORWARD"|"BLOCK"]
			- LISTEN: 			/* Not learning or relaying frames. */
			- LEARN: 			/* Learning but not relaying frames. */
			- FORWARD:			/* Learning and relaying frames. */
			- BLOCK:			/* Not part of spanning tree. */

* portConfig:
	down: boolean				/* Port is administratively down. */
	no_stp: boolean				/* Disable 802.1D spanning tree on port. */
	no_recv: boolean			/* Drop all packets except 802.1D spanning tree packets. */
	no_recv_stp: boolean		/* Drop received 802.1D STP packets. */
	no_flood: boolean			/* Do not include this port when flooding. */
	no_fwd: boolean				/* Drop packets forwarded to port. */
	no_packet_in: boolean		/* Do not send packet-in msgs for port. */
	 
* portFeatures:

	f_10MBHD: boolean			/* 10 Mb half-duplex rate support. */
	f_10MBFD: boolean 			/* 10 Mb full-duplex rate support. */
	f_100MBHD: boolean			/* 100 Mb half-duplex rate support. */
	f_100MBFD: boolean			/* 100 Mb full-duplex rate support. */
	f_1GBHD: boolean			/* 1 Gb half-duplex rate support. */
	f_1GBFD: boolean			/* 1 Gb full-duplex rate support. */
	f_10GBFD: boolean			/* 10 Gb full-duplex rate support. */
	copper: boolean				/* Copper medium. */
	fiber: boolean				/* Fiber medium. */
	autoneg: boolean			/* Auto-negotiation. */
	pause: boolean				/* Pause. */
	pause_asym: boolean			/* Asymmetric pause. */

* portDescription:
	port_no: int
	hw_addr: STRING
	name: STRING
	state: portState
	config: portConfig
	curr:  portFeatures
	supported: portFeatures
	advertised: portFeatures
	peer: portFeatures

* switchFeatures:
	num_buffers: int
	num_tables: int
	ports: list of portDescription