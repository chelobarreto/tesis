from tesis.rest.base_handler import BaseHandler

class NIBRest(BaseHandler):
	
	def get(self):
		self.write(self.runtime.nib.to_json())
		self.finish()