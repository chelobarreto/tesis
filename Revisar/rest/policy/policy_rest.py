import logging, tornado
from tornado import httpclient
from tesis.rest.base_handler import BaseHandler

class PolicyRest(BaseHandler):
	
	def get(self):
		self.write(self.runtime.policy().to_netkat())
		self.finish()

	def post(self):
		error_data = {}
		data = tornado.escape.json_decode(self.request.body) 
		netkat_policy = data['policy']
		try:
			error_data['code'] = "OK"
			error_data['message'] = "Error de Sintaxis"
			self.runtime.netkat_update(netkat_policy)
		except httpclient.HTTPError as e:
			if (e.response.code == 400):
				self.set_status(400)
				error_data['code'] = "ERROR"
				error_data['message'] = e.response.reason
			else:
				self.set_status(400)
				error_data['code'] = "ERROR"
				error_data['message'] = e.response.reason
		except Exception as e:
			self.set_status(400)
			error_data['code'] = "ERROR"
			error_data['message'] = e
		self.write(error_data)
		self.finish()

	def options(self):
		self.set_status(204)
		self.finish()