import logging
import tornado
from tesis.rest.nib.nib_rest import NIBRest
from tesis.rest.device.device_rest import DeviceRest
from tesis.rest.policy.policy_rest import PolicyRest


class WebServer(object):
	def __init__(self, runtime, port):
		logging.info("STARTING REST WEB SERVER")
		self.runtime = runtime
		handlers = [
			(r'/nib', NIBRest, {'runtime': self.runtime}),
			(r'/device/([0-9]+)', DeviceRest, {'runtime': self.runtime}),
			(r'/policy', PolicyRest, {'runtime': self.runtime})
		]
		self.app = tornado.web.Application(handlers)
		self.app.listen(8080)
		logging.info("REST WEB SERVER STARTED")